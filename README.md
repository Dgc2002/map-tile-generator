# Doogle Maps Tile Generator

This is a changed copy of [RuneLite](https://github.com/runelite/).
Please do not use this code a your client for OSRS!

This project is part of the Cartographer project from the OSRS/RS Wiki team, also known as Doogle maps and Kartographer on the wiki.

This codebase is used to create the map tiles for these maps.
Basic idea:
- Input: RuneScape client (and cache data)
- Process: This code
- Output: Images of the map and extra data about the map.

Related project:
- [Doogle Maps (Leaflet client)](https://gitlab.com/weirdgloop/doogle-maps)
- [Kartographer for Doogle Maps (wiki integration)](https://gitlab.com/weirdgloop/extensions/kartographer)

Website:
- [View map](https://maps.runescape.wiki/)
- [Project info](https://oldschool.runescape.wiki/w/RuneScape:Map)

## Usage

This code is not automated and still involves some manual changes.

- View, modify and run the file [MapExport.java](/cache/src/main/java/net/runelite/cache/MapExport.java)
- View results.
- Modify the [ProcessRegionsToMapsTiles.sh](/scripts/ProcessRegionsToMapsTiles.sh) to set the right paths.
- Copy files for deployment.

To get more info about this process view the [project info](https://oldschool.runescape.wiki/w/RuneScape:Map)

## TODO

This project contains more code then used. The main parts of the RuneLite code that are used is the 'cache' and 'http-api' (for Xteas keys) parts.

### License

This Code base is licensed under GNU GPLv3.
This code is based on RuneLite (licensed under the BSD 2-clause license)

# RuneLite info

## Project Layout

- [cache](cache/src/main/java/net/runelite/cache) - Libraries used for reading/writing cache files, as well as the data in it
- [http-api](http-api/src/main/java/net/runelite/http/api) - API for api.runelite.net
- [http-service](http-service/src/main/java/net/runelite/http/service) - Service for api.runelite.net
- [runelite-api](runelite-api/src/main/java/net/runelite/api) - RuneLite API, interfaces for accessing the client
- [runelite-mixins](runelite-mixins/src/main/java/net/runelite) - Mixins which are injected into the injected client's classes
- [runescape-api](runescape-api/src/main/java/net/runelite) - Mappings correspond to these interfaces, runelite-api is a subset of this
- [runelite-client](runelite-client/src/main/java/net/runelite/client) - Game client with plugins

## Usage

Open the project in your IDE as a Maven project, build the root module and then run the RuneLite class in runelite-client.  
For more information visit the [RuneLite Wiki](https://github.com/runelite/runelite/wiki).
