/*
 * Copyright (c) 2017, Adam <Adam@sigterm.info>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.cache.definitions.loaders;

import java.util.LinkedList;
import net.runelite.cache.definitions.WorldMapDefinition;
import net.runelite.cache.definitions.WorldMapType0;
import net.runelite.cache.definitions.WorldMapType1;
import net.runelite.cache.definitions.WorldMapType2;
import net.runelite.cache.definitions.WorldMapType3;
import net.runelite.cache.definitions.WorldMapTypeBase;
import net.runelite.cache.io.InputStream;
import net.runelite.cache.region.Position;

public class WorldMapLoader
{
	public WorldMapDefinition load(byte[] b, int fileId)
	{
		WorldMapDefinition def = new WorldMapDefinition();
		InputStream in = new InputStream(b);

		def.fileId = fileId;
		def.safeName = in.readString();
		def.name = in.readString();

		int packedPos = in.readInt();
		if (packedPos == -1)
		{
			def.position = new Position(-1, -1, -1);
		}
		else
		{
			int z = packedPos >> 28 & 3;
			int x = packedPos >> 14 & 16383;
			int y = packedPos & 16383;
			def.position = new Position(x, y, z);
		}

		def.field450 = in.readInt();
		in.readUnsignedByte();
		def.field457 = in.readUnsignedByte() == 1;
		def.field451 = in.readUnsignedByte();
		int var3 = in.readUnsignedByte();
		def.regionlist = new LinkedList();

		for (int var4 = 0; var4 < var3; ++var4)
		{
			def.regionlist.add(this.loadType(in));
		}

		return def;
	}

	private WorldMapTypeBase loadType(InputStream in)
	{
		int worldMapType = in.readUnsignedByte();
		//      field397 = new class27(1, (byte)0);
		//      field390 = new class27(2, (byte)1);
		//      field399 = new class27(3, (byte)2);
		//      field393 = new class27(0, (byte)3);
		WorldMapTypeBase base;
		switch (worldMapType)
		{
			case 0:
				// type 1
				base = load1(in);
				break;
			case 1:
				// type 2
				base = load2(in);
				break;
			case 2:
				// type 3
				base = load3(in);
				break;
			case 3:
				// type 0
				base = load0(in);
				break;
			default:
				throw new IllegalStateException();
		}
		return base;
	}

	private WorldMapTypeBase load0(InputStream in)
	{
		WorldMapType0 wm = new WorldMapType0();

		wm.field606 = in.readUnsignedByte();
		wm.field605 = in.readUnsignedByte();
		wm.field601 = in.readUnsignedShort();
		wm.field602 = in.readUnsignedByte();
		wm.field603 = in.readUnsignedShort();
		wm.field607 = in.readUnsignedByte();
		wm.field604 = in.readUnsignedShort();
		wm.field600 = in.readUnsignedByte();
		wm.field608 = in.readUnsignedShort();
		wm.field609 = in.readUnsignedByte();

		return wm;
	}

	private WorldMapTypeBase load1(InputStream in)
	{
		WorldMapType1 wm = new WorldMapType1();

		wm.alwaysZero = in.readUnsignedByte();
		wm.numberOfPlanes = in.readUnsignedByte();
		wm.lowerLeftX = in.readUnsignedShort();
		wm.lowerLeftY = in.readUnsignedShort();
		wm.lowerRightX = in.readUnsignedShort();
		wm.upperLeftY = in.readUnsignedShort();
		wm.upperLeftX = in.readUnsignedShort();
		wm.lowerRightY = in.readUnsignedShort();
		wm.upperRightX = in.readUnsignedShort();
		wm.upperRightY = in.readUnsignedShort();

		return wm;
	}

	private WorldMapTypeBase load2(InputStream in)
	{
		WorldMapType2 wm = new WorldMapType2();

		wm.z1 = in.readUnsignedByte();
		wm.field511 = in.readUnsignedByte();
		wm.x1 = in.readUnsignedShort();
		wm.y1 = in.readUnsignedShort();
		wm.x2 = in.readUnsignedShort();
		wm.y2 = in.readUnsignedShort();

		return wm;
	}

	private WorldMapTypeBase load3(InputStream in)
	{
		WorldMapType3 wm = new WorldMapType3();

		wm.field377 = in.readUnsignedByte();
		wm.field387 = in.readUnsignedByte();
		wm.oldX = in.readUnsignedShort();
		wm.chunk_oldXLow = in.readUnsignedByte();
		wm.chunk_oldXHigh = in.readUnsignedByte();
		wm.oldY = in.readUnsignedShort();
		wm.chunk_oldYLow = in.readUnsignedByte();
		wm.chunk_oldYHigh = in.readUnsignedByte();
		wm.newX = in.readUnsignedShort();
		wm.chunk_newXLow = in.readUnsignedByte();
		wm.chunk_newXHigh = in.readUnsignedByte();
		wm.newY = in.readUnsignedShort();
		wm.chunk_newYLow = in.readUnsignedByte();
		wm.chunk_newYHigh = in.readUnsignedByte();

		return wm;
	}
}
